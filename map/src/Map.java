import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class Map {

	private ArrayList <Settlement> settlements;			
	private ArrayList <Road> roads;
								
	public Map() {
		settlements = new ArrayList<Settlement>();
		roads = new ArrayList<Road>();
	}

	/**
	 * In this version we display the result of calling toString on the command
	 * line. Future versions may display graphically
	 */
	 
	public void display() {
		System.out.println(toString());
	}
	public void deleteRoad(Road r){
		roads.remove(r);
	}
//////////////////////////////////////////////////////////////////////////////////////////////
	public void addSettlement(Settlement newSettlement) throws IllegalArgumentException {
		if(settlements.contains(newSettlement)){
			System.out.println("ERROR, SETTLEMENT ALLREDY EXIST !!!");
			throw new IllegalArgumentException();
		}
		else{
			settlements.add(newSettlement);	
		}
		
	}
///////////////////////////////////////////////////////////////////////////////////////////////	
	public void addRoad(Road newRoad) throws IllegalArgumentException {
		
		if(roads.contains(newRoad)){
			System.out.println("ERROR, ROAD ALLREDY EXIST !!!");
			throw new IllegalArgumentException();
		}
		else{
			roads.add(newRoad);	
		}	
	}
////////////////////////////////////////////////////////////////////////////////////////////////
	public Settlement findSettlement(String name){
		
		Settlement foundSettelment = null;
		for (Settlement s : settlements) {
			if(s.getName().equals(name)){
				foundSettelment = s;
			}
		}
		return foundSettelment;
	} 
///////////////////////////////////////////////////////////////////////////////////////////////
	
	public void load() {
	
	}
///////////////////////////////////////////////////////////////////////////////////////////////
	public void save() throws FileNotFoundException {
		
		PrintWriter writeIntoFile = new PrintWriter("RoadMapppppp.txt");
		writeIntoFile.println(this.toString());
		writeIntoFile.close();
		
		System.out.println("SAVE DONE");
	}
///////////////////////////////////////////////////////////////////////////////////////////////
	public String toString(){
		
		String result = "";
		result += "List of settlements and theirs roads:\n"+"*************************\n"
		+"*************************\n";
		
		for(Settlement s: settlements){		//petla po settlements
			result += "Settlement Name: " + s.getName()+"\n"+"Population: " 
			+ s.getPopulation()+"\n"+"Kind: "+s.getKind()+"\n\n";
			
			
				for(Road r: roads){
					
					if(r.getSourceSettlement().equals(s)){	//if source of the road is a this settlement in external loop print 
															//info about roads under this settlement
						
						result += "\n"+"Road Name: " + r.getName()+"\n"+"Lenght: " 
						+ r.getLength()+"\n"+"Clasification: "+r.getClassification()+"\n"+
						"Source = "+r.getSourceSettlement().getName()+"\n"+
						"Destination = "+r.getDestinationSettlement().getName()+"\n\n";	
						}
				}
						result+="*********end of the roads for this settlement**********\n\n";
		}
		
		return result;
	}
////////////////////////////////////////////////////////////////////////////////////////////////	
}
