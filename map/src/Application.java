

import java.io.FileNotFoundException;
import java.util.Scanner;

import com.sun.xml.internal.ws.api.model.ParameterBinding.Kind;


public class Application {

	private Scanner scan;
	private Map map;

	public Application() {
		scan = new Scanner(System.in);
		map = new Map();
	}

	private void runMenu() throws FileNotFoundException {
		
		int choice;
		System.out.println("*******************************************\n"
						 + "**** WELCOME TO ROAD MAP OF CAREDIGION ****\n"
						 + "*******************************************");
		do {
			printMenu();
			choice = scan.nextInt();
			switch (choice) {
			case 1:
				System.out.println("1: Create Settlement");
				createSettlement();		//call the function crate settlement
				System.out.println("****************************************");
				break;
			
			case 2:
				System.out.println("2: Delete Settlement");
				deleteSettlement();		//call the function deleteSettlement but what does it mean delete ?
				System.out.println("****************************************");
				break;
		
			case 3:
				System.out.println("3: Create Road");
				createRoad();			//call the function create road
				System.out.println("****************************************");
				break;
		
			case 4:									
				System.out.println("4: Delete Road");
				deleteRoad();
				//System.out.println("not implemented jet");
				System.out.println("****************************************");
				break;
				
			case 5:									
				System.out.println("5: Display Map");
				map.display();			
				System.out.println("****************************************");
				break;
				
			case 6:									
				System.out.println("6: Save");
				this.save();
				System.out.println("****************************************");
				break;
			
			case 7:
				System.out.println("7: Quit");
				break;

			default:
				System.out.println("not a valid choice try again !!!");
				System.out.println("****************************************");
			}
		} while ( choice != 7 );

	}//end runMenu

	private void createRoad() {
		
		String roadName = "";
		String roadClassification = "";
		Classification kindOfClassification ;
		double lenght = 0;
		
		System.out.println("GIVE ME YOUR ROAD NAME");
		roadName = scan.next().toUpperCase();
		
		System.out.println("GIVE ME YOUR ROAD CLLASIFICATION [M|A|B|U]");
		roadClassification = scan.next().toUpperCase();
		kindOfClassification =  Classification.valueOf(roadClassification);
		
		System.out.println("GIVE ME YOUR ROAD LENGHT");
		lenght = scan.nextDouble();
		
		System.out.println("GIVE ME YOUR SOURCE SEETTLEMENT NAME");
		String sourceSettlementName = scan.next().toUpperCase();
		Settlement source = map.findSettlement(sourceSettlementName);  
		
		System.out.println("GIVE ME YOUR DESTINATION SETTLEMENT NAME");
		String destinationSettlementName = scan.next().toUpperCase();
		Settlement destination = map.findSettlement(destinationSettlementName);
		
		Road road = new Road (roadName, kindOfClassification, source, destination, lenght);
		map.addRoad(road);
		
	}
	
	private void deleteRoad() {
		//havent done it should be bassicaly as above
	}
	
	private void createSettlement(){
		String name = "";
		String settlementType = "";
		SettlementType kindOfSettlement;
		int population = 0;
		
		System.out.println("GIVE ME YOUR SETTLEMENT NAME");
		name = scan.next().toUpperCase();
		
		System.out.println("GIVE ME YOUR SETTLEMENT TYPE [CITY|TOWN|VILLAGE|HAMLET]");
		settlementType = scan.next().toUpperCase();
		kindOfSettlement = SettlementType.valueOf(settlementType);  //string settlementType jako input
																	//pomysl nad tym czy sa cudzyslowy czy nie
																	//kindOfSettlement = SettlementType.valueOf(scan.next()); alternatively
		System.out.println("GIVE ME YOUR SETTLEMENT POPULATION");
		population = scan.nextInt();
		
		Settlement settlement = new Settlement (name, kindOfSettlement);
		settlement.setPopulation(population);
		
		map.addSettlement(settlement);
	}
	
	private void deleteSettlement() {
		
	}
	
/*	private Classification askForRoadClassifier() {
		Classification result = null;
		boolean valid;
		do{
			valid = false;
			System.out.print("Enter a road classification: ");
			for (Classification cls: Classification.values()){
				System.out.print(cls + " ");
			}
			String choice = scan.nextLine().toUpperCase();
			try {
				result = Classification.valueOf(choice);
				valid = true;
			} catch (IllegalArgumentException iae){
				System.out.println(choice + " is not one of the options. Try again.");
			}
		}while(!valid);
		return result;
	}*/
	
	private void save() throws FileNotFoundException {
		map.save();
	}

	private void load() {
		map.load();
	}

	private void printMenu() {
	
		System.out.println("menu lines: \n "
				+ "1 - Create Setlement \n "
				+ "2 - Delete Setlement \n "
				+ "3 - Create Road \n "
				+ "4 - Delete Road \n"
				+ "5 - Display Map \n"
				+ "6 - Save Map \n"
				+ "7 - Quit \n");
	}

	public static void main(String args[]) throws FileNotFoundException {
		Application app = new Application();
		app.load();
		app.runMenu();
		app.save();
	}

}
